package sample

import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.scene.text.Text

class Controller {

    @FXML
    var actionTarget = Text()

    @FXML
    fun handleSubmit(event: ActionEvent) {
        actionTarget.text = "Sign in button pressed!"
    }


}