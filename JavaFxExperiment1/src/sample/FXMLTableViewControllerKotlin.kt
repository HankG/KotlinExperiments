package sample

import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.scene.control.TableView
import javafx.scene.control.TextField

class FXMLTableViewControllerKotlin {

    @FXML
    var tableView: TableView<PersonKotlin>? = null;

    @FXML
    var firstNameField: TextField? = null

    @FXML
    var lastNameField: TextField? = null

    @FXML
    var emailField: TextField? = null

    @FXML
    private fun addPerson(event: ActionEvent) {
        val data = tableView!!.items
        data.add(PersonKotlin(firstNameField!!.text,
                lastNameField!!.text,
                emailField!!.text))
        firstNameField!!.text = ""
        lastNameField!!.text = ""
        emailField!!.text = ""
    }

}
