package sample;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

public class FXMLTableViewControllerJava {
    @FXML
    private TableView<PersonJava> tableView;

    @FXML
    private TextField firstNameField;

    @FXML
    private TextField lastNameField;

    @FXML
    private TextField emailField;

    @FXML
    protected void addPerson(ActionEvent event) {
        ObservableList<PersonJava> data = tableView.getItems();
        data.add(new PersonJava(firstNameField.getText(),
                lastNameField.getText(),
                emailField.getText()));
        firstNameField.setText("");
        lastNameField.setText("");
        emailField.setText("");
    }

}
