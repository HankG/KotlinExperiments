package sample

import javafx.beans.property.SimpleStringProperty

data class Person(var firstName: String = "", var lastName: String = "", var email: String = "");

class PersonKotlin2(firstName: String = "", lastName: String = "", email: String = "") {
    private val firstNameProperty = SimpleStringProperty("")
    private val lastNameProperty = SimpleStringProperty("")
    private val emailProperty = SimpleStringProperty("")

    private val person = Person()

    var firstName: String
        get() = firstNameProperty.get()
        set(value) {
            firstNameProperty.set(value)
            person.firstName = value
        }

    var lastName: String
        get() = lastNameProperty.get()
        set(value) {
            lastNameProperty.set(value)
            person.lastName = value
        }

    var email: String
        get() = emailProperty.get()
        set(value) {
            emailProperty.set(value)
            person.email = value
        }

    init {
        this.firstName = firstName
        this.lastName = lastName
        this.email = email
    }
}
