package sample

import javafx.event.ActionEvent
import javafx.fxml.FXML
import javafx.scene.control.TableView
import javafx.scene.control.TextField

class FXMLTableViewControllerKotlin2 {

    @FXML
    var tableView: TableView<PersonKotlin2> = TableView<PersonKotlin2>()

    @FXML
    var firstNameField: TextField = TextField()

    @FXML
    var lastNameField: TextField = TextField()

    @FXML
    var emailField: TextField = TextField()

    @FXML
    private fun addPerson(event: ActionEvent) {
        val data = tableView.items
        data.add(PersonKotlin2(firstNameField.text,
                lastNameField.text,
                emailField.text))
        firstNameField.text = ""
        lastNameField.text = ""
        emailField.text = ""
    }

}