package sample

import javafx.beans.property.SimpleStringProperty


class PersonKotlin(firstName: String = "", lastName: String = "", email: String = "") {
    private val firstNameProperty = SimpleStringProperty("")
    private val lastNameProperty = SimpleStringProperty("")
    private val emailProperty = SimpleStringProperty("")

    var firstName:String
        get() = firstNameProperty.get()
        set(value) = firstNameProperty.set(value)

    var lastName:String
        get() = lastNameProperty.get()
        set(value) = lastNameProperty.set(value)

    var email:String
        get() = emailProperty.get()
        set(value) = emailProperty.set(value)


    init {
        this.firstNameProperty.set(firstName)
        this.lastNameProperty.set(lastName)
        this.emailProperty.set(email)
    }


}
